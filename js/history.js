history = [];
history_counter = 0;
pointer = 0;

function saveHistory(imgData) {
	if (history_counter === 0){
		var imgTag = document.getElementById('main-img'),
			canvas = document.getElementById('main-canvas');

		var originalData = getImageData(imgTag, canvas);
		history[history_counter] = originalData;
		history_counter++;
	}
	history[history_counter] = imgData;
	history_counter++;
	pointer = history_counter;
}

function undo(canvas) {
	if (pointer > 0) {
		var ctx = canvas.getContext("2d");
		ctx.putImageData(history[--pointer], 0, 0);
		console.log("Undo: "+pointer);
	}	
}


function redo(canvas) {
	if (pointer < history_counter-1) {
		var ctx = canvas.getContext("2d");
		ctx.putImageData(history[++pointer], 0, 0);
		console.log("Redo: "+pointer);
	}	
}

function reset(canvas) {
	var ctx = canvas.getContext("2d");
	ctx.putImageData(history[0], 0, 0);
	pointer = 0;
	console.log("Reset: "+pointer);
}