function setImageURL(imgObj, imgID, canvasID, containerID){

	var imgType = /image.*/;

	if (imgObj.type.match(imgType)) {

		var reader = new FileReader();
		var imgTag = document.getElementById(imgID);

		reader.onload = function(e){
			imgTag.src = reader.result;
			setTimeout(function () {
				loadImage (imgID, canvasID, containerID);
			}, 0);
		}

		reader.readAsDataURL(imgObj);
	} 

	else {
		imgTag.innerHTML = "File not supported!"
	}
}