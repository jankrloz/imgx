function setImageCanvas (imgTag, canvas, maxWidth, maxHeight){

	resizeCanvasImage(imgTag, canvas, maxWidth, maxHeight);
	/*
	imgTag.width = canvas.width;
	imgTag.height = canvas.height;
	*/

	brightnessSlider(-100, 100);
	thresholdSlider(0, 255);

	canvas.addEventListener('mousemove', function(evt) {

		var mousePos = getMousePos(canvas, evt);
		var message = 'x: ' + mousePos.x + ', y: ' + mousePos.y;
		$('#coords').html(message);
	}, false);
}
