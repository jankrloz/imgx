var history = [];

$('.histogram').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData(imgTag, canvas);

	calculateHistogram(imgData);

	histogram_window = window.open("histogram.html","Histograma","width = 1024, height = 500");

});

$('.equalize').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData(imgTag, canvas);

	var data = calculateHistogram(imgData);
	var equalize = [];

	for (var k = 0; k < 255; k += 4){
		for (var j = 0; j < k; j += 4){
	    	equalize[k] = parseInt((255/imgData.data.length)*data[j]);
	    }
	}
	localStorage["histogram_data"] = JSON.stringify(equalize);
	histogram_window = window.open("histogram.html","Histograma","width = 1024, height = 500");
});

$('.negative').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = negative (imgData, ctx);

	ctx.putImageData(filterData, 0, 0);

});

$('.grayscale').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = grayscale(imgData, ctx);

	ctx.putImageData(filterData, 0, 0);

});

$('.sharpen').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = convolute (imgData, ctx, [ 0, -1,  0,
											   -1,  5, -1,
												0, -1,  0]);
												
	ctx.putImageData(filterData, 0, 0);

});

$('.average').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = convolute (imgData, ctx, [ 1/9, 1/9, 1/9,
											    1/9, 1/9, 1/9,
											    1/9, 1/9, 1/9 ]);

	ctx.putImageData(filterData, 0, 0);

});

$('.weighted-average').click(function () {

	var w;
		bootbox.prompt({
			title: "Weight",
			callback: function(result) {
			    if (result !== null) {
			      	w = parseInt(result);
			      	var imgTag = document.getElementById('main-img'),
						canvas = document.getElementById('main-canvas');

					var imgData = getImageData (imgTag, canvas);
					saveHistory(imgData);
					var ctx = canvas.getContext("2d");

					var filterData = convolute (imgData, ctx, [ 1/(w+8), 1/(w+8), 1/(w+8),
														  		1/(w+8), w/(w+8), 1/(w+8),
														  		1/(w+8), 1/(w+8), 1/(w+8) ]);
			    }
			}
		});

});

$('.gaussian-blur').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = convolute (imgData, ctx, [ 1/273, 4/273, 7/273, 4/273, 1/273,
											  4/273, 16/273, 26/273, 16/273, 4/273,
											  7/273, 26/273, 41/273, 26/273, 7/273,
											  4/273, 16/273, 26/273, 16/273, 4/273,
											  1/273, 4/273, 7/273, 4/273, 1/273]);

	ctx.putImageData(filterData, 0, 0);
});

$('.laplacian').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = convolute (imgData, ctx, [ 1, -1, 1,
											   -1,  4,-1,
											    1, -1, 1 ]);

	ctx.putImageData(filterData, 0, 0);
});

$('.sobel-border').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = convolute (imgData, ctx, [ 2/9, 1/9, 0,
												1/9, 0, 1/9,
												0, 1/9, 2/9 ]);

	ctx.putImageData(filterData, 0, 0);
});

$('.soften').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = convolute (imgData, ctx, [ 1/16, 2/16, 1/16,
												2/16, 4/16, 2/16,
												1/16, 2/16, 1/16 ]);

	ctx.putImageData(filterData, 0, 0);
});

$('.sobel').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = grayscale (imgData, ctx);

	var vertical = convoluteFloat32(filterData, ctx,
	[-1,-2,-1,
	  0, 0, 0,
	  1, 2, 1]);
	var horizontal = convoluteFloat32(filterData, ctx,
	[1, 0, -1,
	 2, 0, -2,
	 1, 0, -1]);

	var id = createImageData(vertical.width, vertical.height);
	for (var i=0; i<id.data.length; i+=4) {
		var v = Math.abs(vertical.data[i]);
		id.data[i] = v;
		var h = Math.abs(horizontal.data[i]);
		id.data[i+1] = h
		id.data[i+2] = (v+h)/4;
		id.data[i+3] = 255;
	}

	ctx.putImageData(id, 0, 0);

});

$('.prewitt').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = grayscale (imgData, ctx);

	var vertical = convoluteFloat32(filterData, ctx,
	[-1,-1,-1,
	  0, 0, 0,
	  1, 1, 1]);
	var horizontal = convoluteFloat32(filterData, ctx,
	[1, 0, -1,
	 2, 0, -2,
	 1, 0, -1]);

	var id = createImageData(vertical.width, vertical.height);
	for (var i=0; i<id.data.length; i+=4) {
		var v = Math.abs(vertical.data[i]);
		id.data[i] = v;
		var h = Math.abs(horizontal.data[i]);
		id.data[i+1] = h
		id.data[i+2] = (v+h)/3;
		id.data[i+3] = 255;
	}

	ctx.putImageData(id, 0, 0);
});

$('.roberts').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var filterData = grayscale (imgData, ctx);

	var vertical = convoluteFloat32(filterData, ctx,
	[-1, 0, 0,
	  0, 1, 0,
	  0, 0, 0]);
	var horizontal = convoluteFloat32(filterData, ctx,
	[ 0, 0,-1,
	  0, 1, 0,
	  0, 0, 0]);

	var id = createImageData(vertical.width, vertical.height);
	for (var i=0; i<id.data.length; i+=4) {
		var v = Math.abs(vertical.data[i]);
		id.data[i] = v;
		var h = Math.abs(horizontal.data[i]);
		id.data[i+1] = h
		id.data[i+2] = (v+h);
		id.data[i+3] = 255;
	}

	ctx.putImageData(id, 0, 0);
});

function brightnessSlider(minvalue, maxvalue){

	var canvas = document.getElementById('main-canvas');
	var ctx = canvas.getContext('2d');
	var img = document.getElementById('main-img');
	var imgData = ctx.getImageData(0, 0, img.width, img.height);
	var $slider = $(".bs");

	var brightnessvalue = 0;

  	$slider.slider({
	    min: minvalue,
	    max: maxvalue,
	    value: 0,
	    orientation: "horizontal",
	    range: "min",
	    slide: function(event, ui) {
	        $(".popover-title").html('Brightness: '+ui.value);
	        brightnessvalue = ui.value;
	        var filterData = brightnessAdjust (imgData, ctx, currentImgData, brightnessvalue);
	        ctx.putImageData(filterData, 0, 0);
	    }
  	});

	$('.brightness').popover({trigger: 'manual'}).click(function() {
        var $this = $(this);

        currentImgData = ctx.getImageData(0, 0, img.width, img.height);
        
        if ($this.toggleClass('active').hasClass('active')) {
            $this.popover('show');
            $('.bs').show();
            $slider.width(250);

            $('.popover-content')
                .empty()
                .append($slider)
                .width(400)
                .append('<button id="applyBrightness" class="btn btn-inverse btn-sm">Apply</button>')
                .append('<button id="cancelBrightness" class="btn btn-inverse btn-sm">Cancel</button>');

            $('#applyBrightness').click(function(){
            	$slider.detach();
            	$this.popover('hide');
            	$slider.slider({value: 0});
            	imgData = ctx.getImageData(0, 0, img.width, img.height);
        		saveHistory(imgData);     		
		    });

		    $('#cancelBrightness').click(function(){
		    	setImgData(imgData, ctx, currentImgData);
		    	$slider.detach();
		    	$this.popover('hide');
		    	$slider.slider({value: 0});
		    });

        } else {
            $slider.detach();
            $this.popover('hide');
        }
    });
}

function thresholdSlider(minvalue, maxvalue){

	var canvas = document.getElementById('main-canvas');
	var ctx = canvas.getContext('2d');
	var img = document.getElementById('main-img');
	var imgData = ctx.getImageData(0, 0, img.width, img.height);
	var $slider = $(".us");

	var umbralvalue = 0;

  	$slider.slider({
	    min: minvalue,
	    max: maxvalue,
	    value: 100,
	    orientation: "horizontal",
	    range: "min",
	    slide: function(event, ui) {
	        $(".popover-title").html('Umbral: '+ui.value);
	        umbralvalue = ui.value;
	        var filterData = thresholdImage (imgData, ctx, currentImgData, umbralvalue);
	        ctx.putImageData(filterData, 0, 0);
	    }
  	});

	$('.threshold').popover({trigger: 'manual'}).click(function() {
        var $this = $(this);

        currentImgData = ctx.getImageData(0, 0, img.width, img.height);
        
        if ($this.toggleClass('active').hasClass('active')) {
            $this.popover('show');
            $('.us').show();
            $slider.width(250);

            $('.popover-content')
                .empty()
                .append($slider)
                .width(400)
                .append('<button id="applyThreshold" class="btn btn-inverse btn-sm">Apply</button>')
                .append('<button id="cancelThreshold" class="btn btn-inverse btn-sm">Cancel</button>');

            $('#applyThreshold').click(function(){
            	$slider.detach();
            	$this.popover('hide');
            	$slider.slider({value: 100});
            	imgData = ctx.getImageData(0, 0, img.width, img.height);
        		saveHistory(imgData);      		
		    });

		    $('#cancelThreshold').click(function(){
		    	setImgData(imgData, ctx, currentImgData);
		    	$slider.detach();
		    	$this.popover('hide');
		    	$slider.slider({value: 100});
		    });

        } else {
            $slider.detach();
            $this.popover('hide');
        }
    });
}

$('.custom-kernel').click(function(e) {
	var kernel = [];
    bootbox.dialog({
    	message: 
    			'<input type="text" style="width: 36px" name="k1">'+
	    		'<input type="text" style="width: 36px" name="k2">'+
		    	'<input type="text" style="width: 36px" name="k3"><br/>'+
		    	'<input type="text" style="width: 36px" name="k4">'+
		    	'<input type="text" style="width: 36px" name="k5">'+
		    	'<input type="text" style="width: 36px" name="k6"><br/>'+
		    	'<input type="text" style="width: 36px" name="k7">'+
		    	'<input type="text" style="width: 36px" name="k8">'+
		    	'<input type="text" style="width: 36px" name="k9">',
  	    title: "Custom Kernel",
  	    buttons: {
            success: {
                label: "Save",
                className: "btn-success",
                callback: function () {
                    kernel.push($("input[name='k1']").val());
                    kernel.push($("input[name='k2']").val());
                    kernel.push($("input[name='k3']").val());
                    kernel.push($("input[name='k4']").val());
                    kernel.push($("input[name='k5']").val());
                    kernel.push($("input[name='k6']").val());
                    kernel.push($("input[name='k7']").val());
                    kernel.push($("input[name='k8']").val());
                    kernel.push($("input[name='k9']").val());
                    
                    var imgTag = document.getElementById('main-img'),
						canvas = document.getElementById('main-canvas');

					var imgData = getImageData (imgTag, canvas);
					saveHistory(imgData);
					var ctx = canvas.getContext("2d");

					var filterData = convolute (imgData, ctx, kernel);

					ctx.putImageData(filterData, 0, 0);
                }
            }
        }
    });
});




$('.and-operator').click(function () {

	var imgTag1 = document.getElementById('main-img'),
		imgTag2 = document.getElementById('second-img'),
		canvas1 = document.getElementById('main-canvas'),
		canvas2 = document.getElementById('second-canvas');

	var imgData1 = getImageData (imgTag1, canvas1);
	var imgData2 = getImageData (imgTag2, canvas2);
	saveHistory(imgData1);

	var ctx1 = canvas1.getContext("2d");
	var ctx2 = canvas2.getContext("2d");

	var filterData = andOperator (imgData1, ctx1, imgData2, ctx2);

	ctx1.putImageData(filterData, 0, 0);

});

$('.or-operator').click(function () {

	var imgTag1 = document.getElementById('main-img'),
		imgTag2 = document.getElementById('second-img'),
		canvas1 = document.getElementById('main-canvas'),
		canvas2 = document.getElementById('second-canvas');

	var imgData1 = getImageData (imgTag1, canvas1);
	var imgData2 = getImageData (imgTag2, canvas2);
	saveHistory(imgData1);

	var ctx1 = canvas1.getContext("2d");
	var ctx2 = canvas2.getContext("2d");

	var filterData = orOperator (imgData1, ctx1, imgData2, ctx2);

	ctx1.putImageData(filterData, 0, 0);

});

$('.plus-operator').click(function () {

	var imgTag1 = document.getElementById('main-img'),
		imgTag2 = document.getElementById('second-img'),
		canvas1 = document.getElementById('main-canvas'),
		canvas2 = document.getElementById('second-canvas');

	var imgData1 = getImageData (imgTag1, canvas1);
	var imgData2 = getImageData (imgTag2, canvas2);
	saveHistory(imgData1);

	var ctx1 = canvas1.getContext("2d");
	var ctx2 = canvas2.getContext("2d");

	var filterData = plusOperator (imgData1, ctx1, imgData2, ctx2);

	ctx1.putImageData(filterData, 0, 0);

});

$('.less-operator').click(function () {

	var imgTag1 = document.getElementById('main-img'),
		imgTag2 = document.getElementById('second-img'),
		canvas1 = document.getElementById('main-canvas'),
		canvas2 = document.getElementById('second-canvas');

	var imgData1 = getImageData (imgTag1, canvas1);
	var imgData2 = getImageData (imgTag2, canvas2);
	saveHistory(imgData1);

	var ctx1 = canvas1.getContext("2d");
	var ctx2 = canvas2.getContext("2d");

	var filterData = lessOperator (imgData1, ctx1, imgData2, ctx2);

	ctx1.putImageData(filterData, 0, 0);

});

$('.multiply-operator').click(function () {

	var imgTag1 = document.getElementById('main-img'),
		imgTag2 = document.getElementById('second-img'),
		canvas1 = document.getElementById('main-canvas'),
		canvas2 = document.getElementById('second-canvas');

	var imgData1 = getImageData (imgTag1, canvas1);
	var imgData2 = getImageData (imgTag2, canvas2);
	saveHistory(imgData1);

	var ctx1 = canvas1.getContext("2d");
	var ctx2 = canvas2.getContext("2d");

	var filterData = multiplyOperator (imgData1, ctx1, imgData2, ctx2);

	ctx1.putImageData(filterData, 0, 0);

});

$('.divide-operator').click(function () {

	var imgTag1 = document.getElementById('main-img'),
		imgTag2 = document.getElementById('second-img'),
		canvas1 = document.getElementById('main-canvas'),
		canvas2 = document.getElementById('second-canvas');

	var imgData1 = getImageData (imgTag1, canvas1);
	var imgData2 = getImageData (imgTag2, canvas2);
	saveHistory(imgData1);

	var ctx1 = canvas1.getContext("2d");
	var ctx2 = canvas2.getContext("2d");

	var filterData = divideOperator (imgData1, ctx1, imgData2, ctx2);

	ctx1.putImageData(filterData, 0, 0);

});

$('.additive-noise').click(function(event) {
	var p;
	bootbox.prompt({
		title: "Percentage",
		callback: function(result) {
		    if (result !== null) {
		      	p = parseFloat(result);
		      	var imgTag = document.getElementById('main-img'),
					canvas = document.getElementById('main-canvas');

				var imgData = getImageData (imgTag, canvas);
				saveHistory(imgData);
				var ctx = canvas.getContext("2d");

				var filterData = additiveNoise(imgData, ctx, p/1000);
				ctx.putImageData(filterData, 0, 0);
		    }
		}
	});
});

$('.substractive-noise').click(function(event) {
	var p;
	bootbox.prompt({
		title: "Percentage",
		callback: function(result) {
		    if (result !== null) {
		      	p = parseFloat(result);
		      	var imgTag = document.getElementById('main-img'),
					canvas = document.getElementById('main-canvas');

				var imgData = getImageData (imgTag, canvas);
				saveHistory(imgData);
				var ctx = canvas.getContext("2d");

		      	var filterData = substractiveNoise(imgData, ctx, p/1000);
				ctx.putImageData(filterData, 0, 0);
		    }
		}
	});
});

$('.mix-noise').click(function(event) {
	var p;
	bootbox.prompt({
		title: "Percentage",
		callback: function(result) {
		    if (result !== null) {
		      	p = parseFloat(result);

		      	var imgTag = document.getElementById('main-img'),
					canvas = document.getElementById('main-canvas');

				var imgData = getImageData (imgTag, canvas);
				saveHistory(imgData);
				var ctx = canvas.getContext("2d");

		      	var filterData = mixNoise(imgData, ctx, p/1000);
				ctx.putImageData(filterData, 0, 0);
		    }
		}
	});
});

$('.median-filter').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var filterData = avgFilter (imgData, ctx, 
		[ 1, 1, 1,
		  1, 1, 1,
		  1, 1, 1 ]);

	ctx.putImageData(filterData, 0, 0);		
});

$('.max-filter').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var filterData = maxFilter (imgData, ctx, 
		[ 1, 1, 1,
		  1, 1, 1,
		  1, 1, 1 ]);
  	
	ctx.putImageData(filterData, 0, 0);		
});

$('.min-filter').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var filterData = minFilter (imgData, ctx, 
		[ 1, 1, 1,
		  1, 1, 1,
		  1, 1, 1 ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.median-filter').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var filterData = medianFilter (imgData, ctx, 
		[ 1, 1, 1,
		  1, 1, 1,
		  1, 1, 1 ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.moda-filter').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var filterData = modaFilter (imgData, ctx, 
		[ 1, 1, 1,
		  1, 1, 1,
		  1, 1, 1 ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.dilatacion').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var filterData = dilatacion (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.erosion').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var filterData = erosion (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.apertura').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var eData = erosion (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var filterData = dilatacion (eData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.cierre').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var dData = dilatacion (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var filterData = erosion (dData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.dilataciongray').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var filterData = dilatacionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.erosiongray').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var filterData = erosionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.aperturagray').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var eData = erosionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var filterData = dilatacionGray (eData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.cierregray').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var dData = dilatacionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var filterData = erosionGray (dData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.alisamiento').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var eData = erosionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var dData = dilatacionGray (eData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var e2Data = dilatacionGray (dData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var filterData = erosionGray (e2Data, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.extraccion').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var eData = erosion (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var filterData = lessOperator(imgData, ctx, eData, ctx);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.extracciongray').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var eData = erosionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

	var filterData = lessOperator(imgData, ctx, eData, ctx);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.tophat').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var eData = erosionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var dData = dilatacionGray (eData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

	var filterData = lessOperator(dData, ctx, imgData, ctx);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.bothat').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var dData = dilatacionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	eData = erosionGray (dData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

	var filterData = lessOperator(eData, ctx, imgData, ctx);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.gradienteplus').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var eData = erosionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

	var filterData = lessOperator(imgData, ctx, eData, ctx);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.gradienteless').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var dData = dilatacionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var filterData = lessOperator(dData, ctx, imgData, ctx);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.gradientesimetrico').click(function()
{
	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

  	var dData = dilatacionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

  	var eData = erosionGray (imgData, ctx, 
		[ 'x',1,'x',
		   1 ,1, 1,
		  'x',1,'x' ]);

	var filterData = lessOperator(dData, ctx, eData, ctx);
  	
	ctx.putImageData(filterData, 0, 0);	
});

$('.hitandmiss').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var e1 = erosion (imgData, ctx, 
		[ 1, 1, 1,
		 'x',1,'x',
		  0, 0, 0 ]);
	var e2 = erosion (imgData, ctx, 
		['x',0, 0,
		  1, 1, 0,
		  1, 1,'x' ]);
	var e3 = erosion (imgData, ctx, 
		[ 1,'x',0,
		  1, 1, 0,
		  1,'x',0]);
	var e4 = erosion (imgData, ctx, 
		[ 1, 1,'x',
		  1, 1, 0,
		 'x',0, 0]);
	var e5 = erosion (imgData, ctx, 
		[ 1, 1, 1,
		 'x',1,'x',
		  0, 0, 0 ]);
	var e6 = erosion (imgData, ctx, 
		['x',1, 1,
		  0, 1, 1,
		  0, 0,'x' ]);
	var e7 = erosion (imgData, ctx, 
		[ 0,'x',1,
		  0, 1, 1,
		  0,'x',1]);
	var e8 = erosion (imgData, ctx, 
		[ 0, 0,'x',
		  0, 1, 1,
		 'x',1, 1 ]);

	var d;
	for (var i = 0; i < imgData.data.length; i++) {
		imgData.data[i] = e1.data[i] | e2.data[i] | e3.data[i] | e4.data[i] | e5.data[i] | e6.data[i] | e7.data[i] | e8.data[i];
	}

	ctx.putImageData(imgData, 0, 0);
});

$('.hitandmissgray').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var e1 = erosionGray (imgData, ctx, 
		[ 1, 1, 1,
		 'x',1,'x',
		  0, 0, 0 ]);
	var e2 = erosionGray (imgData, ctx, 
		['x',0, 0,
		  1, 1, 0,
		  1, 1,'x' ]);
	var e3 = erosionGray (imgData, ctx, 
		[ 1,'x',0,
		  1, 1, 0,
		  1,'x',0]);
	var e4 = erosionGray (imgData, ctx, 
		[ 1, 1,'x',
		  1, 1, 0,
		 'x',0, 0]);
	var e5 = erosionGray (imgData, ctx, 
		[ 1, 1, 1,
		 'x',1,'x',
		  0, 0, 0 ]);
	var e6 = erosionGray (imgData, ctx, 
		['x',1, 1,
		  0, 1, 1,
		  0, 0,'x' ]);
	var e7 = erosionGray (imgData, ctx, 
		[ 0,'x',1,
		  0, 1, 1,
		  0,'x',1]);
	var e8 = erosionGray (imgData, ctx, 
		[ 0, 0,'x',
		  0, 1, 1,
		 'x',1, 1 ]);

	var d;
	for (var i = 0; i < imgData.data.length; i++) {
		imgData.data[i] = e1.data[i] | e2.data[i] | e3.data[i] | e4.data[i] | e5.data[i] | e6.data[i] | e7.data[i] | e8.data[i];
	}

	ctx.putImageData(imgData, 0, 0);
});

$('.adelgazamiento').click(function () {

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var imgData = getImageData (imgTag, canvas);
	saveHistory(imgData);
	var ctx = canvas.getContext("2d");

	var e1 = erosionHit (imgData, ctx, 
		[ 1, 1, 1,
		 'x',1,'x',
		  0, 0, 0 ]);
	var e2 = erosionHit (e1, ctx, 
		['x',0, 0,
		  1, 1, 0,
		  1, 1,'x' ]);
	var e3 = erosionHit (e2, ctx, 
		[ 1,'x',0,
		  1, 1, 0,
		  1,'x',0]);
	var e4 = erosionHit (e3, ctx, 
		[ 1, 1,'x',
		  1, 1, 0,
		 'x',0, 0]);
	var e5 = erosionHit (e4, ctx, 
		[ 1, 1, 1,
		 'x',1,'x',
		  0, 0, 0 ]);
	var e6 = erosionHit (e5, ctx, 
		['x',1, 1,
		  0, 1, 1,
		  0, 0,'x' ]);
	var e7 = erosionHit (e6, ctx, 
		[ 0,'x',1,
		  0, 1, 1,
		  0,'x',1]);
	var e8 = erosionHit (e7, ctx, 
		[ 0, 0,'x',
		  0, 1, 1,
		 'x',1, 1 ]);

	ctx.putImageData(e8, 0, 0);
});

$('.undo').click(function () {
	canvas = document.getElementById('main-canvas');
	undo(canvas);
});

$('.redo').click(function () {
	canvas = document.getElementById('main-canvas');
	redo(canvas);
});


$('.reset-to-original').click(function () {
	canvas = document.getElementById('main-canvas');
	reset(canvas);
});

