$(document).ready(function() {
	var imgID = 'main-img',
		inputID = 'main-input',
		canvasID = 'main-canvas',
		containerID = 'main-space';

	setInputHandleEvent(imgID, inputID, canvasID, containerID);

	imgID = 'second-img',
	inputID = 'second-input',
	canvasID = 'second-canvas',
	containerID = 'second-space';

	setInputHandleEvent(imgID, inputID, canvasID, containerID);

});

$('#open-menu, #btn-begin').click(function () {

	var inputID = 'main-input';

	var inputTag = document.getElementById(inputID);

	inputTag.click();

	$('#home').fadeOut();
	$('#workspace').fadeIn(2000);
	$('body').css('backgroundImage', 'none');
	$('body').css('backgroundColor', '#121A21');

	history = [];
	history_counter = 0;
	pointer = 0;


});

$('#open-second').click(function () {

	var inputID = 'second-input';

	var inputTag = document.getElementById(inputID);

	inputTag.click();
	
	$('#main-space').removeClass('col-md-11').addClass('col-md-5');
	// $('#right-bar').removeClass('col-md-2').addClass('col-md-3');
	$('#second-space').fadeIn();

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var maxWidth = $('#main-space').width();
	var maxHeight = $('#main-space').height();

	setImageCanvas (imgTag, canvas, maxWidth, maxHeight);
	
});

$('#close-second').click(function () {
	$('#main-space').removeClass('col-md-5').addClass('col-md-11');
	// $('#right-bar').removeClass('col-md-3').addClass('col-md-2');
	$('#second-space').fadeOut();

	var imgTag = document.getElementById('main-img'),
		canvas = document.getElementById('main-canvas');

	var maxWidth = $('#main-space').width();
	var maxHeight = $('#main-space').height();

	setImageCanvas (imgTag, canvas, maxWidth, maxHeight);
});

$('.about-imgx').click(function(e) {
    bootbox.dialog({
    	title: "Designed & Developed by jankrloz © ® 2015<br/>",
    	message: "Juan Carlos Navarrete Gordillo<br/>"+
    	"2012630518<br/>"+
    	"ESCOM-IPN<br/>"
    });
});

$('.navbar-brand').click(function() {
	location.reload();
});

function getMousePos(canvas, evt) {

	var rect = canvas.getBoundingClientRect();
	return {
		x: parseInt(evt.clientX - rect.left),
		y: parseInt(evt.clientY - rect.top)
	};
}