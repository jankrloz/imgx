function getImageData (imgTag, canvas){

	var ctx = canvas.getContext("2d");
	return ctx.getImageData(0, 0, imgTag.width, imgTag.height);
}

function setImgDataToLocalStorage(imgData, localStorageName){
	var data = [];

	for (var i = 0; i < imgData.data.length; i++) {
		data [i] = imgData.data[i];
	}

	localStorage[localStorageName] = JSON.stringify(data);
}