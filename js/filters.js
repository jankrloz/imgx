var tmpCanvas = document.createElement('canvas');
var tmpCtx = tmpCanvas.getContext('2d');

function createImageData (w,h) {
	return tmpCtx.createImageData(w,h);
};

function getImgData (imgData, ctx)
{

	var copy = [];
	copy.length = imgData.data.length;

	for (var i = 0; i < imgData.data.length; i+=4)
	{
	    copy.push(imgData.data[i]);
	    copy.push(imgData.data[i+1]);
	    copy.push(imgData.data[i+2]);
	}

	return copy;
}

function setImgData(imgData, ctx, newImgData){
	imgData = newImgData;
	ctx.putImageData(imgData, 0, 0);
}

function andOperator(imgData1, ctx1, imgData2, ctx2) {
	if (imgData1.width === imgData2.width && imgData1.height === imgData2.height){
		for (var i = 0; i < imgData1.data.length; i+=4)
		{
		    imgData1.data[i] = imgData1.data[i] & imgData2.data[i];
		    imgData1.data[i+1] = imgData1.data[i+1] & imgData2.data[i+1];
		    imgData1.data[i+2] = imgData1.data[i+2] & imgData2.data[i+2];
		}

		return imgData1;
	}
	else{
		alert('Not the same');
	}
}

function orOperator(imgData1, ctx1, imgData2, ctx2) {
	if (imgData1.width === imgData2.width && imgData1.height === imgData2.height){
		for (var i = 0; i < imgData1.data.length; i+=4)
		{
		    imgData1.data[i] = imgData1.data[i] | imgData2.data[i];
		    imgData1.data[i+1] = imgData1.data[i+1] | imgData2.data[i+1];
		    imgData1.data[i+2] = imgData1.data[i+2] | imgData2.data[i+2];
		}

		return imgData1;
	}
	else{
		alert('Not the same');
	}
}

function plusOperator(imgData1, ctx1, imgData2, ctx2) {
	if (imgData1.width === imgData2.width && imgData1.height === imgData2.height){
		for (var i = 0; i < imgData1.data.length; i+=4)
		{
		    imgData1.data[i] += imgData2.data[i];
		    imgData1.data[i+1] += imgData2.data[i+1];
		    imgData1.data[i+2] += imgData2.data[i+2];
		}

		return imgData1;
	}
	else{
		alert('Not the same');
	}
}

function lessOperator(imgData1, ctx1, imgData2, ctx2) {
	if (imgData1.width === imgData2.width && imgData1.height === imgData2.height){
		for (var i = 0; i < imgData1.data.length; i+=4)
		{
		    imgData1.data[i] -= imgData2.data[i];
		    imgData1.data[i+1] -= imgData2.data[i+1];
		    imgData1.data[i+2] -= imgData2.data[i+2];
		}

		return imgData1;
	}
	else{
		alert('Not the same');
	}
}

function multiplyOperator(imgData1, ctx1, imgData2, ctx2) {
	if (imgData1.width === imgData2.width && imgData1.height === imgData2.height){
		for (var i = 0; i < imgData1.data.length; i+=4)
		{
		    imgData1.data[i] *= imgData2.data[i];
		    imgData1.data[i+1] *= imgData2.data[i+1];
		    imgData1.data[i+2] *= imgData2.data[i+2];
		}

		return imgData1;
	}
	else{
		alert('Not the same');
	}
}

function divideOperator(imgData1, ctx1, imgData2, ctx2) {
	if (imgData1.width === imgData2.width && imgData1.height === imgData2.height){
		for (var i = 0; i < imgData1.data.length; i+=4)
		{
		    imgData1.data[i] /= imgData2.data[i];
		    imgData1.data[i+1] /= imgData2.data[i+1];
		    imgData1.data[i+2] /= imgData2.data[i+2];
		}

		return imgData1;
	}
	else{
		alert('Not the same');
	}
}

function negative (imgData, ctx){

	for (var i = 0; i < imgData.data.length; i+=4)
	{
	    imgData.data[i] = 255 - imgData.data[i];
	    imgData.data[i+1] = 255 - imgData.data[i+1];
	    imgData.data[i+2] = 255 - imgData.data[i+2];
	}

	return imgData;
}

function grayscale (imgData, ctx) {

	for (var i=0; i<imgData.data.length; i+=4) {
		var avg = (imgData.data[i] + imgData.data[i+1] + imgData.data[i+2]) / 3;
	      imgData.data[i]     = avg; // red
	      imgData.data[i + 1] = avg; // green
	      imgData.data[i + 2] = avg; // blue
	}

	return imgData;
};

function brightnessAdjust (imgData, ctx, currentImgData, value){

	//var brightness = parseInt(prompt ("Input brightness level (-100 to 100)"));
	var brightness = parseInt(value);

	if (brightness >= -100 && brightness <= 100)
	{
		for (var i = 0; i < imgData.data.length; i+=4)
		{
		    imgData.data[i] = currentImgData.data[i] + brightness;
		    imgData.data[i+1] = currentImgData.data[i+1] + brightness;
		    imgData.data[i+2] = currentImgData.data[i+2] + brightness;
		}
	}

	return imgData;
}

function thresholdImage (imgData, ctx, currentImgData, threshold)
{

	if (threshold >= 0 && threshold <= 255)
	{
		for (var i = 0; i < imgData.data.length; i+=4)
		{
		    var v = ((currentImgData.data[i] + currentImgData.data[i+1] + currentImgData.data[i+2])/3 >= threshold) ? 255 : 0;
			imgData.data[i] = imgData.data[i+1] = imgData.data[i+2] = v
		}
	}
	
	else
	{
		alert("Wrong umbral value");
	}

	return imgData;
}

function additiveNoise (imgData, ctx, percentage){
	var num = parseInt(imgData.data.length*percentage);
	var rand;
	
	for (var i = num; i > 0; i--) {
		rand = Math.floor(Math.random()*imgData.data.length);
		rand -= (rand % 4);
		imgData.data[rand] = 255;
		imgData.data[rand+1] = 255;
		imgData.data[rand+2] = 255;
	}

	return imgData;
}

function substractiveNoise (imgData, ctx, percentage){
	var num = parseInt(imgData.data.length*percentage);
	var rand;
	
	for (var i = num; i > 0; i--) {
		rand = Math.floor(Math.random()*imgData.data.length);
		rand -= (rand % 4);
		imgData.data[rand] = 0;
		imgData.data[rand+1] = 0;
		imgData.data[rand+2] = 0;
	}

	return imgData;
}

function mixNoise (imgData, ctx, percentage){
	var num = parseInt(imgData.data.length*percentage);
	
	for (var i = num/2; i > 0; i--) {
		//For additive
		rand = Math.floor(Math.random()*imgData.data.length);
		rand -= (rand % 4);
		imgData.data[rand] = 255;
		imgData.data[rand+1] = 255;
		imgData.data[rand+2] = 255;

		//For Substractive
		rand = Math.floor(Math.random()*imgData.data.length);
		rand -= (rand % 4);
		imgData.data[rand] = 0;
		imgData.data[rand+1] = 0;
		imgData.data[rand+2] = 0;
	}

	return imgData;
}

function convolute (imgData, ctx, weights, opaque) {
	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var r=0, g=0, b=0, a=0;
			for (var cy=0; cy<side; cy++) {
				for (var cx=0; cx<side; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					r += src[srcOff] * wt;
					g += src[srcOff+1] * wt;
					b += src[srcOff+2] * wt;
					a += src[srcOff+3] * wt;
				}
			}
			dst[dstOff] = r;
			dst[dstOff+1] = g;
			dst[dstOff+2] = b;
			dst[dstOff+3] = a + alphaFac*(255-a);
		}
	}

	return output;
};

if (!window.Float32Array)
	Float32Array = Array;

function convoluteFloat32 (imgData, ctx, weights, opaque) {
	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = {
		width: w, height: h, data: new Float32Array(w*h*4)
	};
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var r=0, g=0, b=0, a=0;
			for (var cy=0; cy<side; cy++) {
				for (var cx=0; cx<side; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					r += src[srcOff] * wt;
					g += src[srcOff+1] * wt;
					b += src[srcOff+2] * wt;
					a += src[srcOff+3] * wt;
				}
			}
			dst[dstOff] = r;
			dst[dstOff+1] = g;
			dst[dstOff+2] = b;
			dst[dstOff+3] = a + alphaFac*(255-a);
		}
	}

	return output;
};

function avgFilter (imgData, ctx, weights, opaque) {
	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var r=0, g=0, b=0;
			for (var cy=0; cy<side; cy++) {
				for (var cx=0; cx<side; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					r += src[srcOff] * wt;
					g += src[srcOff+1] * wt;
					b += src[srcOff+2] * wt;
				}
			}
			r /= weights.length;
			g /= weights.length;
			b /= weights.length;
			dst[dstOff] = r;
			dst[dstOff+1] = g;
			dst[dstOff+2] = b;
			dst[dstOff+3] = 255;
		}
	}

	return output;
};

function maxFilter (imgData, ctx, weights, opaque) {
	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var r=0, g=0, b=0, a=0;
			var maxR = [], maxG = [], maxB = [];
			for (var cy=0; cy<side; cy++) {
				for (var cx=0; cx<side; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					r = src[srcOff] * wt;
					g = src[srcOff+1] * wt;
					b = src[srcOff+2] * wt;
					maxR.push(r);
					maxG.push(g);
					maxB.push(b);
				}
			}
			maxR = Math.max.apply(null, maxR);
			maxG = Math.max.apply(null, maxG);
			maxB = Math.max.apply(null, maxB);
			dst[dstOff] = maxR;
			dst[dstOff+1] = maxG;
			dst[dstOff+2] = maxB;
			dst[dstOff+3] = 255;
		}
	}

	return output;
};

function minFilter (imgData, ctx, weights, opaque) {
	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var r=0, g=0, b=0, a=0;
			var minR = [], minG = [], minB = [];
			for (var cy=0; cy<side; cy++) {
				for (var cx=0; cx<side; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					r = src[srcOff] * wt;
					g = src[srcOff+1] * wt;
					b = src[srcOff+2] * wt;
					minR.push(r);
					minG.push(g);
					minB.push(b);
				}
			}
			minR = Math.min.apply(null, minR);
			minG = Math.min.apply(null, minG);
			minB = Math.min.apply(null, minB);
			dst[dstOff] = minR;
			dst[dstOff+1] = minG;
			dst[dstOff+2] = minB;
			dst[dstOff+3] = 255;
		}
	}

	return output;
};

function medianFilter (imgData, ctx, weights, opaque) {
	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var r=0, g=0, b=0, a=0;
			var medianR = [], medianG = [], medianB = [];
			for (var cy=0; cy<side; cy++) {
				for (var cx=0; cx<side; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					r = src[srcOff] * wt;
					g = src[srcOff+1] * wt;
					b = src[srcOff+2] * wt;
					medianR.push(r);
					medianG.push(g);
					medianB.push(b);
				}
			}
			medianR = median (medianR);
			medianG = median (medianG);
			medianB = median (medianB);
			dst[dstOff] = medianR;
			dst[dstOff+1] = medianG;
			dst[dstOff+2] = medianB;
			dst[dstOff+3] = 255;
		}
	}

	return output;
};

function modaFilter (imgData, ctx, weights, opaque) {
	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var r=0, g=0, b=0, a=0;
			var modaR = [], modaG = [], modaB = [];
			for (var cy=0; cy<side; cy++) {
				for (var cx=0; cx<side; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					r = src[srcOff] * wt;
					g = src[srcOff+1] * wt;
					b = src[srcOff+2] * wt;
					modaR.push(r);
					modaG.push(g);
					modaB.push(b);
				}
			}
			modaR = moda (modaR);
			modaG = moda (modaG);
			modaB = moda (modaB);
			dst[dstOff] = modaR;
			dst[dstOff+1] = modaG;
			dst[dstOff+2] = modaB;
			dst[dstOff+3] = 255;
		}
	}

	return output;
};


/*
function erosion (imgData, ctx, weights, opaque) {

	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var pass = true;
			for (var cy=0; cy<side && pass; cy++) {
				for (var cx=0; cx<side && pass; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					if (src[srcOff]*wt != src[srcOff]){
						pass = false;
					}
					if (wt == 'x'){
						pass = true;
					}
				}
			}
			if (pass){
				dst[dstOff] = 0;
				dst[dstOff+1] = 0;
				dst[dstOff+2] = 0;
				dst[dstOff+3] = 255;
			}
			else {
				dst[dstOff] = src[dstOff];
				dst[dstOff+1] = src[dstOff+1];
				dst[dstOff+2] = src[dstOff+2];
				dst[dstOff+3] = 255;
			}
		}
	}

	return output;
	
};


function dilatacion (imgData, ctx, weights, opaque) {

	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var pass = true;
			for (var cy=0; cy<side && pass; cy++) {
				for (var cx=0; cx<side && pass; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					if (src[srcOff]*wt != src[srcOff]){
						pass = false;
					}
					if (wt == 'x'){
						pass = true;
					}
				}
			}
			if (!pass){
				
				dst[dstOff] = 255;
				dst[dstOff+1] = 255;
				dst[dstOff+2] = 255;
				dst[dstOff+3] = 255;
			}
			else {
				dst[dstOff] = src[dstOff];
				dst[dstOff+1] = src[dstOff+1];
				dst[dstOff+2] = src[dstOff+2];
				dst[dstOff+3] = 255;
			}
		}
	}

	return output;
};

function erosionGray (imgData, ctx, weights, opaque) {

	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var pass = true;
			var min = [];
			for (var cy=0; cy<side && pass; cy++) {
				for (var cx=0; cx<side && pass; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					min.push(src[srcOff]);
					if (src[srcOff]*wt != src[srcOff]){
						pass = false;
					}
					if (wt == 'x'){
						pass = true;
					}
				}
			}
			if (!pass){
				var m = Math.min.apply(null, min);
				dst[dstOff] = m;
				dst[dstOff+1] = m;
				dst[dstOff+2] = m;
				dst[dstOff+3] = 255;				
			}
			else {
				dst[dstOff] = src[dstOff];
				dst[dstOff+1] = src[dstOff+1];
				dst[dstOff+2] = src[dstOff+2];
				dst[dstOff+3] = 255;
			}
		}
	}

	return output;
};

function dilatacionGray (imgData, ctx, weights, opaque) {
	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var pass = true;
			var max = [];
			for (var cy=0; cy<side && pass; cy++) {
				for (var cx=0; cx<side && pass; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					max.push(src[srcOff]);
					if (src[srcOff]*wt != src[srcOff]){
						pass = false;
					}
					if (wt == 'x'){
						pass = true;
					}
				}
			}
			if (!pass){
				var m = Math.max.apply(null, max);
				dst[dstOff] = m;
				dst[dstOff+1] = m;
				dst[dstOff+2] = m;
				dst[dstOff+3] = 255;
			}
			else {
				dst[dstOff] = src[dstOff];
				dst[dstOff+1] = src[dstOff+1];
				dst[dstOff+2] = src[dstOff+2];
				dst[dstOff+3] = 255;
			}
		}
	}

	return output;
	
};

*/

function erosion (imgData, ctx, weights, opaque) {

	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var pass = true;
			for (var cy=0; cy<side && pass; cy++) {
				for (var cx=0; cx<side && pass; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					var k = (wt == 1) ? 255 : 0;
					if (src[srcOff] != k){
						pass = false;
					}
					if (wt == 'x'){
						pass = true;
					}
					// alert(src[srcOff]+'*'+wt+' != '+src[srcOff]+' ? '+pass+' ('+src[srcOff]*wt+')');
				}
			}
			if (pass){
				dst[dstOff] = src[dstOff];
				dst[dstOff+1] = src[dstOff+1];
				dst[dstOff+2] = src[dstOff+2];
				dst[dstOff+3] = 255;
			}
			else {
				dst[dstOff] = 0;
				dst[dstOff+1] = 0;
				dst[dstOff+2] = 0;
				dst[dstOff+3] = 255;
			}
		}
	}

	return output;
	
};

function dilatacion (imgData, ctx, weights, opaque) {

	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var pass = true;
			dst[dstOff] = src[dstOff];
			dst[dstOff+1] = src[dstOff+1];
			dst[dstOff+2] = src[dstOff+2];
			dst[dstOff+3] = 255;
			for (var cy=0; cy<side && pass; cy++) {
				for (var cx=0; cx<side && pass; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					if (src[dstOff] == 255 && wt == 1){
						dst[srcOff] = 255;
						dst[srcOff+1] = 255;
						dst[srcOff+2] = 255;
						dst[srcOff+3] = 255;
						//alert(src[dstOff]+' == '+255+' && '+wt+' == 1');
					}
					// alert(src[srcOff]+'*'+wt+' != '+src[srcOff]+' ? '+pass+' ('+src[srcOff]*wt+')');
				}
			}
		}
	}

	return output;
	
};

function dilatacionGray (imgData, ctx, weights, opaque) {

	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var pass = true;
			dst[dstOff] = src[dstOff];
			dst[dstOff+1] = src[dstOff+1];
			dst[dstOff+2] = src[dstOff+2];
			dst[dstOff+3] = 255;
			var max = [];
			for (var cy=0; cy<side && pass; cy++) {
				for (var cx=0; cx<side && pass; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					max.push(src[srcOff]);
				}
			}
			for (var cy=0; cy<side && pass; cy++) {
				for (var cx=0; cx<side && pass; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					if (src[dstOff] > 0 && wt == 1){
						var m = Math.max.apply(null, max);
						dst[srcOff] = m;
						dst[srcOff+1] = m;
						dst[srcOff+2] = m;
						dst[srcOff+3] = 255;
					}
				}
			}
		}
	}

	return output;
	
};

function erosionGray (imgData, ctx, weights, opaque) {

	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var pass = true;
			var min = [];
			for (var cy=0; cy<side && pass; cy++) {
				for (var cx=0; cx<side && pass; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					var k = (wt == 1) ? 1 : 0;
					min.push(src[srcOff]);
					if ((src[srcOff] <= 127 && k == 1) || (src[srcOff] > 127 && k == 0)){
						pass = false;
					}
					if (wt == 'x'){
						pass = true;
					}
					// alert(src[srcOff]+'*'+wt+' != '+src[srcOff]+' ? '+pass+' ('+src[srcOff]*wt+')');
				}
			}
			if (pass){
				dst[dstOff] = src[dstOff];
				dst[dstOff+1] = src[dstOff+1];
				dst[dstOff+2] = src[dstOff+2];
				dst[dstOff+3] = 255;
			}
			else {
				var m = Math.min.apply(null, min);
				dst[dstOff] = m;
				dst[dstOff+1] = m;
				dst[dstOff+2] = m;
				dst[dstOff+3] = 255;
			}
		}
	}

	return output;
	
};

function erosionHit (imgData, ctx, weights, opaque) {

	var side = Math.round(Math.sqrt(weights.length));
	var halfSide = Math.floor(side/2);

	var src = imgData.data;
	var sw = imgData.width;
	var sh = imgData.height;

	var w = sw;
	var h = sh;
	var output = createImageData(w, h);
	var dst = output.data;

	var alphaFac = opaque ? 1 : 0;

	for (var y=0; y<h; y++) {
		for (var x=0; x<w; x++) {
			var sy = y;
			var sx = x;
			var dstOff = (y*w+x)*4;
			var pass = true;
			for (var cy=0; cy<side && pass; cy++) {
				for (var cx=0; cx<side && pass; cx++) {
					var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
					var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
					var srcOff = (scy*sw+scx)*4;
					var wt = weights[cy*side+cx];
					var k = (wt == 1) ? 255 : 0;
					if (src[srcOff] != k){
						pass = false;
					}
					if (wt == 'x'){
						pass = true;
					}
					// alert(src[srcOff]+'*'+wt+' != '+src[srcOff]+' ? '+pass+' ('+src[srcOff]*wt+')');
				}
			}
			if (!pass){
				dst[dstOff] = src[dstOff];
				dst[dstOff+1] = src[dstOff+1];
				dst[dstOff+2] = src[dstOff+2];
				dst[dstOff+3] = 255;
			}
			else {
				dst[dstOff] = 0;
				dst[dstOff+1] = 0;
				dst[dstOff+2] = 0;
				dst[dstOff+3] = 255;
			}
		}
	}

	return output;
	
};

function median(values) {
 
    values.sort( function(a,b) {return a - b;} );
 
    var half = Math.floor(values.length/2);
 
    if(values.length % 2)
        return values[half];
    else
        return parseInt((values[half-1] + values[half]) / 2.0);
}

function moda(array){
	//iniciamos las variables necesarias en todo el codigo
	var moda, moda2;
	var contador = 0, contador2 = 0;
	//Recorremos la array
	for (var x=0; x<array.length; x++){
		//Miramos que el numero cogido no sea el de la moda
		if(array[x] != moda){
			var contadorReinicia=0;
			//Recorremos la array para encontrar concordancias on el numero sacado de la array de X
			for(var i=0; i<array.length; i++){
				//cunado el numero sea igual al de la array de x le añadimos 1 al contador
				if (array[i] == array[x]) contadorReinicia++;
			}
			//si el contador que se reinicia nos da mas alto que el contador general añadimos el numero a la variable moda y cambiamos el contador general por el que reinicia
			if (contadorReinicia>contador){
				contador = contadorReinicia;
				moda = array[x];
			}
		}
	}
	//Retornamos la moda!!!
	return moda;
}