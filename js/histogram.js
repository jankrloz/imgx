function calculateHistogram(imgData) {

	var data = [];
    var r = [];
    var g = [];
    var b = [];

    localStorage.clear();

	//Default value of array = 0
	for (i = 0; i < 256; i++) {
        data.push(0);
        r.push(0);
        g.push(0);
        b.push(0);
    }

	for (var i = 0; i < imgData.data.length; i += 4)
	{
	    var max = parseInt((imgData.data[i]+imgData.data[i+1]+imgData.data[i+2])/3);
	    data[max]++;
        var red = imgData.data[i]; r[red]++;
        var green = imgData.data[i+1]; g[green]++;
        var blue = imgData.data[i+2]; b[blue]++;
	}

	localStorage["histogram_data"] = JSON.stringify(data);
    localStorage["histogram_r"] = JSON.stringify(r);
    localStorage["histogram_g"] = JSON.stringify(g);
    localStorage["histogram_b"] = JSON.stringify(b);

    return data;
}

function drawHistogram(histogramID) {

	var chartData = [];

	var data = JSON.parse(localStorage["histogram_data"]);
    var r = JSON.parse(localStorage["histogram_r"]);
    var g = JSON.parse(localStorage["histogram_g"]);
    var b = JSON.parse(localStorage["histogram_b"]);

	for (var i = 1; i < data.length; i++) {
		chartData.push({
            
			"category": i,
			"gs": data[i]
        });
        chartData.push({
            
			"category": i,
			"r": r[i]
        });
        chartData.push({
            
			"category": i,
			"g": g[i]
        });
        chartData.push({
            
			"category": i,
			"b": b[i]
        });
	};

	AmCharts.makeChart(histogramID,
		{
			"type": "serial",
			"categoryField": "category",
			"plotAreaBorderAlpha": 0.2,
			"startDuration": 1,
			"theme": "dark",
			"startEffect": "easeOutSine",
			"sequencedAnimation": false,
			"categoryAxis": {
				"gridPosition": "start"
			},
			"trendLines": [],
			"graphs": [
				{
					"fillAlphas": 0.7,
					"id": "gs-histogram",
					"lineAlpha": 0,
					"fillColors": "#636363",
					"title": "Grayscale Histogram",
					"valueField": "gs"
				},
				{
					"fillAlphas": 0.7,
					"id": "r-histogram",
					"lineAlpha": 0,
					"fillColors": "#D90005",
					"title": "Red Channel Histogram",
					"valueField": "r"
				},
				{
					"fillAlphas": 0.7,
					"id": "g-histogram",
					"lineAlpha": 0,
					"fillColors": "#24B52F",
					"title": "Green Channel Histogram",
					"valueField": "g"
				},
				{
					"fillAlphas": 0.7,
					"id": "b-histogram",
					"lineAlpha": 0,
					"fillColors": "#2665B3",
					"title": "Blue Channel Histogram",
					"valueField": "b"
				},
			],
			"guides": [],
			"valueAxes": [
				{
					"id": "ValueAxis-1",
					"title": "Frequency"
				}
			],
			"allLabels": [],
			"balloon": {},
			"legend": {},
			"titles": [
				{
					"id": "Title-1",
					"size": 15,
					"text": "Histogram"
				}
			],
			"dataProvider": chartData
		}
	);
}

