function loadImage (imgID, canvasID, containerID){

	var imgTag = document.getElementById(imgID),
		canvas = document.getElementById(canvasID);
	
	var maxWidth = $('#'+containerID).width();
	var maxHeight = $('#'+containerID).height();

	setImageCanvas (imgTag, canvas, maxWidth, maxHeight);

	$('#'+canvasID).hide();
	$('#'+canvasID).fadeIn(1000);
}