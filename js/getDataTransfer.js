function setInputHandleEvent(imgID, inputID, canvasID, containerID){
	// Establecer los listeners de click a input.
	$('#'+inputID).change(function (e){
		handleFileSelect (e, imgID, canvasID, containerID);
	});
}

// In Drop & Change
function handleFileSelect(evt, imgID, canvasID, containerID){

	evt.stopPropagation();
	evt.preventDefault();

	var files = evt.target.files || evt.dataTransfer.files; // FileList object.
	var imgObj = files[0];

	setImageURL(imgObj, imgID, canvasID, containerID);
	
}